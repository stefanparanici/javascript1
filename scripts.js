var arr = [1, 2, 'hello', NaN, {
    city: 'IasI',
    zip: null
}, [11, 12], undefined, undefined, undefined]


function addF(x)
{
    return (y)=>{
        return x+y;
    }
}

function getStats()
{
    for(let elemnt in arr)
    {
        console.log(typeof(arr[elemnt]));
    }
}
function limit(fn,max){
    return(x,y)=>{
        while(max!=0)
        {
            max--;
            return fn(x,y);
        }
    }
}
function add1(x, y) {
    return x + y;
}

function whatIsInAName(arg1,arg2){
    let i=0;
    let Matching_Obj= [];
    for(let obj of arg1)
    {
        let inc = 0;
        for(let elemnt1 in Object.keys(obj))
        {
            for(let elemnt2 in Object.keys(arg2))
            {
                if(Object.keys(obj)[elemnt1]==Object.keys(arg2)[elemnt2])
                {
                    if(Object.values(obj)[elemnt1]==Object.values(arg2)[elemnt2])
                        inc++;
                }
            }
        }
        if(inc==Object.keys(arg2).length)
        {
            Matching_Obj[i]=obj;
            i++;
        }
    }

    return Matching_Obj;
}

let limitAdd = limit(add1, 2);
let add = addF(13);


getStats(arr);

console.log(add(10));
console.log(add(-5));


console.log(limitAdd(3, 5)); // 8
console.log(limitAdd(11, 23)); //34
console.log(limitAdd(5, 10)); // undefined

console.log(whatIsInAName([{ first: "Romeo", last: "Montague" }, { first: "Mercutio", last: null }, { first: "Tybalt", last: "Capulet" }], { last: "Capulet" }));
console.log(whatIsInAName([{ "apple": 1 }, { "apple": 1 }, { "apple": 1, "bat": 2 }], { "apple": 1 }));
console.log(whatIsInAName([{"a": 1, "b": 2, "c": 3}], {"a": 1, "b": 9999, "c": 3}));